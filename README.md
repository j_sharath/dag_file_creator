# DAG Script Creator

 - naviagate to extracted folder in terminal
 - type following command in terminal
 - chmod 777 'DAG File Creator'
 - ./'DAG File Creator'
or
 - double click on 'DAG File Creator'

### Creating new setting

1.	Select empty from dropdown list
2.	Click on load button
3.	Specify DAG name and other details

### Loading new script to airflow folder

1.	Specify DAG name and other details
2.	Click on Save DAG Script

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-1.png)

### Saving current setting

1.	Specify DAG name
2.	Click on save

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-2.png)

### Loading saved setting

1.	Select the DAG name from dropdown list
2.	Click on load

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-3.png)

### Deleting saved setting

1.	Select the DAG name from dropdown list
2.	Click on delete

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-4.png)

### Adding new row

1.	Click on Add New Row

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-5.png)

### Clearing all rows

1.	Click on Clear graph

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-6.png)

### Editing row

1.	Click on pencil symbol

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-7.png)

### Deleting row

1.	Click on trash symbol

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-8.png)

### Accepting row changes

1.	Click on accept symbol

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-9.png)

### Discarding row changes

1.	Click on cross symbol

    ![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-10.png)

### Required parameters

The directory of the script files have to be specified in the Tasks Scripts Directory Path section.

The directory path has to specified here (Which by default your home directory).

![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-11.png)

### Specifying email task

The task name in Email section must be same as in the task table.

That is task name specified here 
![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-12.png)

Should be same as here 
![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-13.png)

### Specifying branch task

The task name in Branch section must be same as in the task table.

That is task name specified here 
![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-12.png)

Should be same as here 
![N|Solid](https://raw.githubusercontent.com/autodag/autodag_images/master/mau-13.png)
